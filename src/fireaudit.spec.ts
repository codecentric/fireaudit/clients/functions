import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {FireAudit, FireAuditInversionOfControlContainer} from './fireaudit';
import {Util} from '@endran/firestore-export-import/dist/util';
import {FireAuditUtil} from './services/fireaudit.util';
import {FireAuditExporterService} from './services/fireaudit-exporter.service';
import axios from 'axios';

export class JestMock<T> {
    fn: T | { [p: string]: jest.Mock<any> } | any = {};
    typed = (this.fn as any) as T;

    constructor(...methodNames: string[]) {
        methodNames.forEach(name => (this.fn[name] = jest.fn()));
    }
}

export const testResolve = value => new Promise(resolve => resolve(value));
export const testReject = value => new Promise((_, reject) => reject(value));

describe('FireAudit', () => {

    describe('static fireAudit', () => {
        test('returns fireAudit object', async () => {
            const testDate = new Date();
            const actual = FireAudit.fireAudit('TEST_EVENT', 'TEST_UID', testDate, true);
            expect(actual).toEqual({
                uid: 'TEST_UID',
                event: 'TEST_EVENT',
                deleted: true,
                userTime: testDate,
                serverTime: admin.firestore.FieldValue.serverTimestamp()
            });
        });

        test('returns fireAudit object, with defaults', async () => {
            const actual = FireAudit.fireAudit('TEST_EVENT');

            expect(actual.userTime).toBeDefined();
            delete actual.userTime;
            expect(actual).toEqual({
                uid: 'FUNCTION',
                event: 'TEST_EVENT',
                deleted: false,
                serverTime: admin.firestore.FieldValue.serverTimestamp()
            });
        });
    });


    describe('instance fireAudit', () => {
        let fireAudit: FireAudit;

        let functionsMock: JestMock<any>;
        let adminMock: JestMock<any>;
        let firestoreMock: JestMock<any>;
        let axiosMock: JestMock<any>;

        let ioccMock: JestMock<FireAuditInversionOfControlContainer>;
        let fireAuditExporterServiceMock: JestMock<FireAuditExporterService>;
        let fireAuditUtilMock: JestMock<FireAuditUtil>;

        beforeEach(() => {
            functionsMock = new JestMock('region', 'document', 'onWrite', 'runWith', 'topic', 'onPublish');
            functionsMock.fn.firestore = functionsMock.typed;
            functionsMock.fn.pubsub = functionsMock.typed;
            functionsMock.fn.region.mockReturnValue(functionsMock.typed);
            functionsMock.fn.document.mockReturnValue(functionsMock.typed);
            functionsMock.fn.onWrite.mockReturnValue(functionsMock.typed);
            functionsMock.fn.runWith.mockReturnValue(functionsMock.typed);
            functionsMock.fn.topic.mockReturnValue(functionsMock.typed);
            functionsMock.fn.onPublish.mockReturnValue(functionsMock.typed);

            adminMock = new JestMock('firestore', 'doc', 'delete', 'initializeApp');
            adminMock.fn.firestore.mockReturnValue(adminMock.typed);
            adminMock.fn.doc.mockReturnValue(adminMock.typed);
            adminMock.fn.delete.mockReturnValue(adminMock.typed);
            adminMock.fn.initializeApp.mockReturnValue(adminMock.typed);

            axiosMock = new JestMock('post');

            fireAuditExporterServiceMock = new JestMock('export');
            fireAuditUtilMock = new JestMock('');
            ioccMock = new JestMock('functions', 'axios', 'admin', 'fireAuditExporterService', 'fireAuditUtil', 'firestore', 'adminConfig');
            ioccMock.fn.functions.mockReturnValue(functionsMock.typed);
            ioccMock.fn.axios.mockReturnValue(axiosMock.typed);
            ioccMock.fn.admin.mockReturnValue(adminMock.typed);
            ioccMock.fn.fireAuditExporterService.mockReturnValue(fireAuditExporterServiceMock.typed);
            ioccMock.fn.fireAuditUtil.mockReturnValue(fireAuditUtilMock.typed);
            ioccMock.fn.firestore.mockReturnValue(adminMock.typed);

            fireAudit = new FireAudit(false, ioccMock.typed);
        });

        describe('build', () => {
            test('by default creates eu firestore functions for root, and an export pubsub function', async () => {
                fireAudit.build({levels: 3} as any);

                expect(functionsMock.fn.region).toBeCalledTimes(3 + 1);
                expect(functionsMock.fn.region).toBeCalledWith('europe-west1');

                expect(functionsMock.fn.document).toBeCalledTimes(3);
                expect(functionsMock.fn.document).toBeCalledWith('{col1}/{doc1}');
                expect(functionsMock.fn.document).toBeCalledWith('{col1}/{doc1}/{col2}/{doc2}');
                expect(functionsMock.fn.document).toBeCalledWith('{col1}/{doc1}/{col2}/{doc2}/{col3}/{doc3}');

                expect(functionsMock.fn.onWrite).toBeCalledTimes(3);

                expect(functionsMock.fn.runWith).toBeCalledTimes(1);
                expect(functionsMock.fn.topic).toBeCalledTimes(1);
                expect(functionsMock.fn.runWith).toBeCalledWith({
                    timeoutSeconds: 9 * 60,
                    memory: '2GB'
                });
                expect(functionsMock.fn.topic).toBeCalledTimes(1);
                expect(functionsMock.fn.topic).toBeCalledWith('FIRE_AUDIT_EXPORT');
                expect(functionsMock.fn.onPublish).toBeCalledTimes(1);
            });

            test('creates for non default settings', async () => {
                fireAudit.build({levels: 1, root: 'TEST/ROOT/', region: 'TEST_REGION' as any, domain: 'US'} as any);

                expect(functionsMock.fn.region).toBeCalledTimes(2);
                expect(functionsMock.fn.region).toBeCalledWith('TEST_REGION');

                expect(functionsMock.fn.document).toBeCalledTimes(1);
                expect(functionsMock.fn.document).toBeCalledWith('TEST/ROOT/{col1}/{doc1}');
            });
        });

        describe('writeHandler', () => {
            test('handler write data to repo', async () => {
                fireAudit.build({levels: 1, domain: 'EU', apiKey: 'TEST_API_KEY'});
                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        data: () => {
                            return {TEST: 'BEFORE_CONTENT'};
                        },
                        exists: true
                    },
                    after: {
                        data: () => {
                            return {
                                TEST: 'AFTER_CONTENT',
                                fireAudit: {TEST: 'METADATA_VAL', uid: 'TEST_UID', event: 'TEST_EVENT'}
                            };
                        },
                        exists: true,
                        ref: {path: 'AFTER/PATH'}
                    }
                };

                const context = {
                    timestamp: '2000-03-27T15:05:55.1234567'
                };

                await handler(change, context);

                expect(axiosMock.fn.post).toHaveBeenCalledWith('https://europe-west1-fireaudit-eu.cloudfunctions.net/https-record', {
                    apiKey: 'TEST_API_KEY',
                    data: Util.serializeSpecialTypes({
                        before: {TEST: 'BEFORE_CONTENT'},
                        after: {
                            TEST: 'AFTER_CONTENT',
                            fireAudit: {TEST: 'METADATA_VAL', uid: 'TEST_UID', event: 'TEST_EVENT'}
                        },
                        metadata: {
                            TEST: 'METADATA_VAL',
                            uid: 'TEST_UID',
                            event: 'TEST_EVENT',
                            path: 'AFTER/PATH',
                            auditTime: new admin.firestore.Timestamp(954169555, 123456000) as any
                        }
                    })
                });
            });

            test('handler allows transforming the data', async () => {
                let actualBeforeData: any = {};
                let actualAfterData: any = {};
                let transformCount = 0;
                fireAudit.build({
                    levels: 1, domain: 'US', apiKey: 'TEST_API_KEY',
                    transformer: (data, path, util) => {
                        if (transformCount === 0) {
                            actualBeforeData = data;
                        } else {
                            actualAfterData = data;
                        }
                        transformCount++;
                        expect(util).toEqual(fireAuditUtilMock.typed);
                        expect(path).toEqual('AFTER/PATH');
                        return {TEST: `TRANSFORMED_DATA_${transformCount}`};
                    }
                });
                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        data: () => {
                            return {TEST: 'BEFORE_CONTENT'};
                        },
                        exists: true
                    },
                    after: {
                        data: () => {
                            return {TEST: 'AFTER_CONTENT', fireAudit: {TEST: 'METADATA_VAL'}};
                        },
                        exists: true,
                        updateTime: new admin.firestore.Timestamp(954169555, 654321000),
                        ref: {path: 'AFTER/PATH'}
                    }
                };

                await handler(change, undefined);

                expect(axiosMock.fn.post).toHaveBeenCalledWith(
                    'https://us-central1-fireaudit-us.cloudfunctions.net/https-record',
                    {
                        apiKey: 'TEST_API_KEY',
                        data: Util.serializeSpecialTypes({
                            before: {TEST: 'TRANSFORMED_DATA_1'},
                            after: {TEST: 'TRANSFORMED_DATA_2'},
                            metadata: {
                                uid: 'NO_UID',
                                event: 'UNKNOWN',
                                path: 'AFTER/PATH',
                                auditTime: new admin.firestore.Timestamp(954169555, 654321000) as any
                            }
                        })
                    });

                expect(transformCount).toEqual(2);
                expect(actualBeforeData).toEqual({TEST: 'BEFORE_CONTENT'});
                expect(actualAfterData).toEqual({TEST: 'AFTER_CONTENT', fireAudit: {TEST: 'METADATA_VAL'}});
            });

            test('handler allows to drop data, by returning null before', async () => {
                let transformCount = 0;
                fireAudit.build({
                    levels: 1, domain: 'EU', apiKey: 'TEST_API_KEY',
                    transformer: () => {
                        transformCount++;
                        return transformCount === 1 ? null : undefined;
                    }
                });

                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        data: () => {
                            return {TEST: 'BEFORE_CONTENT'};
                        }
                    },
                    after: {
                        data: () => {
                            return {TEST: 'AFTER_CONTENT', fireAudit: {TEST: 'METADATA_VAL'}};
                        },
                        ref: {path: 'AFTER_PATH'}
                    }
                };

                const context = {
                    timestamp: '2000-03-27T15:05:55.1234567'
                };

                await handler(change, context);

                expect(transformCount).toEqual(1);
                expect(axiosMock.fn.post).toHaveBeenCalledTimes(0);
            });

            test('handler allows to drop data, by returning null after', async () => {
                let transformCount = 0;
                fireAudit.build({
                    levels: 1, domain: 'EU', apiKey: 'TEST_API_KEY',
                    transformer: () => {
                        transformCount++;
                        return transformCount > 1 ? null : undefined;
                    }
                });

                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        data: () => {
                            return {TEST: 'BEFORE_CONTENT'};
                        }
                    },
                    after: {
                        data: () => {
                            return {TEST: 'AFTER_CONTENT', fireAudit: {TEST: 'METADATA_VAL'}};
                        },
                        ref: {path: 'AFTER_PATH'}
                    }
                };

                const context = {
                    timestamp: '2000-03-27T15:05:55.1234567'
                };

                await handler(change, context);

                expect(transformCount).toEqual(2);
                expect(axiosMock.fn.post).toHaveBeenCalledTimes(0);
            });

            test('handler deletes doc when required', async () => {
                fireAudit.build({
                    levels: 1, domain: 'EU', apiKey: 'TEST_API_KEY'
                });

                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        exists: false
                    },
                    after: {
                        data: () => {
                            return {TEST: 'AFTER_CONTENT', fireAudit: {TEST: 'METADATA_VAL', delete: true}};
                        },
                        exists: true,
                        ref: {path: 'AFTER_PATH'},
                        updateTime: new admin.firestore.Timestamp(954169555, 654321000)
                    }
                };

                const context = {
                    timestamp: '2000-03-27T15:05:55.1234567'
                };

                await handler(change, context);
                expect(adminMock.fn.doc).toHaveBeenCalledWith('AFTER_PATH');
                expect(adminMock.fn.delete).toHaveBeenCalledWith({lastUpdateTime: change.after.updateTime});
            });

            test('skips deletes doc when options say so', async () => {
                fireAudit.build({
                    levels: 1, domain: 'EU', apiKey: 'TEST_API_KEY', disableDelete: true
                });

                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        exists: false
                    },
                    after: {
                        data: () => {
                            return {TEST: 'AFTER_CONTENT', fireAudit: {TEST: 'METADATA_VAL', delete: true}};
                        },
                        exists: true,
                        ref: {path: 'AFTER_PATH'},
                        updateTime: new admin.firestore.Timestamp(954169555, 654321000)
                    }
                };

                const context = {
                    timestamp: '2000-03-27T15:05:55.1234567'
                };

                await handler(change, context);
                expect(adminMock.fn.delete).toHaveBeenCalledTimes(0);
            });

            test('can use custom domain', async () => {
                fireAudit.build({
                    levels: 1, domain: 'CUSTOM_DOMAIN', apiKey: 'TEST_API_KEY'
                } as any);

                const handler = functionsMock.fn.onWrite.mock.calls[0][0];

                const change = {
                    before: {
                        data: () => undefined
                    },
                    after: {
                        data: () => undefined,
                        ref: {path: 'AFTER_PATH'}
                    }
                };

                const context = {
                    timestamp: '2000-03-27T15:05:55.1234567'
                };

                await handler(change, context);

                expect(axiosMock.fn.post).toHaveBeenCalledWith(
                    'https://CUSTOM_DOMAIN/https-record',
                    expect.anything());
            });
        });

        describe('export', () => {
            test('passes message attributes to exporter', async () => {
                let data: any = {};
                let dataPath: any = {};
                let util: any = {};

                const transformer = (_data, _dataPath, _util) => {
                    data = _data;
                    dataPath = _dataPath;
                    util = _util;
                };
                const options = {levels: 0, transformer} as any;
                fireAudit.build(options);
                const handler = functionsMock.fn.onPublish.mock.calls[0][0];

                await handler({
                    attributes: {
                        bucketName: 'TEST_BUCKET_NAME',
                        firestorePath: 'TEST_FIRESTORE_PATH',
                        storagePath: 'TEST_STORAGE_PATH',
                        timestamp: 'TEST_TIMESTAMP'
                    }
                });

                expect(ioccMock.fn.fireAuditExporterService).toHaveBeenCalledWith('TEST_BUCKET_NAME');
                expect(ioccMock.fn.fireAuditUtil).toHaveBeenCalledWith(options);
                expect(fireAuditExporterServiceMock.fn.export).toHaveBeenCalledWith('TEST_FIRESTORE_PATH', 'TEST_STORAGE_PATH', 'TEST_TIMESTAMP', expect.anything());

                fireAuditExporterServiceMock.fn.export.mock.calls[0][3]('TEST_DATA', 'TEST_DATA_PATH');

                expect(data).toEqual('TEST_DATA');
                expect(dataPath).toEqual('TEST_DATA_PATH');
                expect(util).toEqual(fireAuditUtilMock.typed);

            });

            test('uses defaults when message attributes are incomplete', async () => {
                const options = {levels: 0} as any;
                fireAudit.build(options);
                const handler = functionsMock.fn.onPublish.mock.calls[0][0];

                await handler({attributes: {}});

                expect(ioccMock.fn.fireAuditExporterService).toHaveBeenCalledWith(undefined);
                expect(ioccMock.fn.fireAuditUtil).toHaveBeenCalledWith(options);
                expect(fireAuditExporterServiceMock.fn.export).toHaveBeenCalledWith('__ROOT__', 'fireAudit', expect.any(String), expect.anything());
            });

            describe('sanitizes root param', () => {
                test('for just /', async () => {
                    const options = {levels: 0, root: '/'} as any;
                    fireAudit.build(options);
                    const handler = functionsMock.fn.onPublish.mock.calls[0][0];
                    await handler({attributes: {}});

                    expect(fireAuditExporterServiceMock.fn.export).toHaveBeenCalledWith('__ROOT__', 'fireAudit', expect.any(String), expect.anything());
                });

                test('with leading and trailing /', async () => {
                    const options = {levels: 0, root: '/someCol/someDoc/'} as any;
                    fireAudit.build(options);
                    const handler = functionsMock.fn.onPublish.mock.calls[0][0];
                    await handler({attributes: {}});

                    expect(fireAuditExporterServiceMock.fn.export).toHaveBeenCalledWith('someCol/someDoc', 'fireAudit', expect.any(String), expect.anything());
                });
            });
        });

        describe('reinitializeApp', () => {
            test('reinits firestore when asked', async () => {
                const config = {name: 'TEST_NAME', options: {projectId: 'TEST_PROJECT_ID'}};
                ioccMock.fn.adminConfig.mockReturnValue(config);

                new FireAudit(true, ioccMock.typed);

                expect(adminMock.fn.initializeApp).toHaveBeenCalledWith(config.options, config.name)
            });
        });

        describe('default iocc', () => {
            test('has production values', async () => {
                const iocc: FireAuditInversionOfControlContainer = (new FireAudit(true) as any).iocc;

                expect(iocc.functions()).toEqual(functions);
                expect(iocc.axios()).toEqual(axios);
                expect(iocc.axios()).toEqual(axios);
                expect(iocc.fireAuditExporterService('test')).toBeDefined();
                expect(iocc.fireAuditUtil({} as any)).toBeDefined();
            });
        });
    });
});
