import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import axios from 'axios';
import {DocumentSnapshot} from 'firebase-functions/lib/providers/firestore';
import {ExportService} from '@endran/firestore-export-import/dist/export.service';
import {FireAuditExporterService} from './services/fireaudit-exporter.service';
import {FireAuditUtil} from './services/fireaudit.util';
import {Util} from '@endran/firestore-export-import/dist/util';
import {FireAuditMetadata, FireAuditMetadataContainer, FireAuditOptions} from './model';

export interface FireAuditInversionOfControlContainer {
    functions: () => any;
    admin: () => any;
    axios: () => any;
    firestore: () => any;
    adminConfig: () => { options?: admin.AppOptions, name?: string }
    fireAuditUtil: (options: FireAuditOptions) => FireAuditUtil;
    fireAuditExporterService: (bucketName: string) => FireAuditExporterService;
}

export class FireAudit {

    static fireAudit(event: string, uid: string = 'FUNCTION', userTime: Date = new Date(), deleted: boolean = false): FireAuditMetadata {
        return {
            uid,
            event,
            userTime,
            deleted,
            serverTime: admin.firestore.FieldValue.serverTimestamp() as any
        };
    }

    private iocc: FireAuditInversionOfControlContainer;

    constructor(reinitializeApp: boolean = false, iocc: Partial<FireAuditInversionOfControlContainer> = {}) {
        this.iocc = {
            functions: () => functions,
            axios: () => axios,
            admin: () => admin,
            adminConfig: () => ({}),
            firestore: () => this.iocc.admin().firestore(),
            fireAuditExporterService: (bucketName: string) => new FireAuditExporterService(new ExportService(this.iocc.firestore()), bucketName),
            fireAuditUtil: (options: FireAuditOptions) => new FireAuditUtil(
                options.hashing?.sha512Salt,
                options.encryption?.encryptionPassword,
                options.encryption?.decryptionPasswords
            ),
            ...iocc
        };

        if (reinitializeApp) {
            const adminConfig = this.iocc.adminConfig();
            this.iocc.admin().initializeApp(adminConfig.options, adminConfig.name);
        }
    }

    public build(options: FireAuditOptions) {
        const res: any = {};

        let index = 1;
        let path = options.root || '';
        while (index <= options.levels) {
            path += `{col${index}}/{doc${index}}`;
            res[`write${index}`] = this.iocc.functions()
                .region(options.region || 'europe-west1')
                .firestore.document(path)
                .onWrite(async (change, context) => await this.writeHandler(change, context, options));
            index++;
            path += '/';
        }

        res['export'] = this.iocc.functions()
            .region(options.region || 'europe-west1')
            .runWith({
                timeoutSeconds: 9 * 60,
                memory: '2GB'
            })
            .pubsub.topic('FIRE_AUDIT_EXPORT')
            .onPublish(async message => await this.pubsubHandler(message, options));

        return res;
    }

    async pubsubHandler(message, options: FireAuditOptions) {
        const bucketName = message.attributes.bucketName;
        const storagePath = message.attributes.storagePath || 'fireAudit';
        const timestamp = message.attributes.timestamp || new Date().toISOString().replace(/:/g, '-');

        let firestorePath: string = message.attributes.firestorePath || options.root || '';
        if (firestorePath.startsWith('/')) {
            firestorePath = firestorePath.substr(1);
        }
        if (firestorePath.endsWith('/')) {
            firestorePath = firestorePath.substr(0, firestorePath.length - 1);
        }
        firestorePath = firestorePath || '__ROOT__';

        const util = this.iocc.fireAuditUtil(options);
        const transformer = options.transformer ? options.transformer : d => d as FireAuditMetadataContainer;

        const exporter = this.iocc.fireAuditExporterService(bucketName);
        await exporter.export(firestorePath, storagePath, timestamp, (data: any, dataPath: string) => transformer(data, dataPath, util));
    }

    async writeHandler(change: functions.Change<DocumentSnapshot>, context: functions.EventContext, options: FireAuditOptions) {
        const util = this.iocc.fireAuditUtil(options);
        const transformer = options.transformer ? options.transformer : d => d as FireAuditMetadataContainer;

        const path = change.after.ref.path;

        const beforeData = transformer(change.before.exists ? change.before.data() : undefined, path, util);
        if (beforeData === null) {
            return;
        }

        const afterData = transformer(change.after.exists ? change.after.data() : undefined, path, util);
        if (afterData === null) {
            return;
        }

        const fireAudit = ((afterData || {}) as any).fireAudit || {};
        const data = {
            before: beforeData,
            after: afterData,
            metadata: {
                ...fireAudit,
                path: path,
                event: fireAudit.event || 'UNKNOWN',
                uid: fireAudit.uid || 'NO_UID',
                auditTime: change.after.updateTime || this.convertTimestamp(context.timestamp)
            }
        };

        const fireAuditPackage = {
            apiKey: options.apiKey,
            data: data
        };

        let url: string;
        switch (options.domain) {
            case 'EU':
                url = 'https://europe-west1-fireaudit-eu.cloudfunctions.net/https-record';
                break;
            case 'US':
                url = 'https://us-central1-fireaudit-us.cloudfunctions.net/https-record';
                break;
            default:
                url = `https://${options.domain}/https-record`;
        }

        await this.iocc.axios().post(url, Util.serializeSpecialTypes(fireAuditPackage));

        if (fireAudit.delete && !options.disableDelete) {
            await this.iocc.firestore().doc(path).delete({lastUpdateTime: change.after.updateTime});
        }
    }

    private convertTimestamp(timestampString: string) {
        if (timestampString && timestampString.length && timestampString.length === 27) {
            const [date, time] = timestampString.split('T');
            const [year, month, day] = date.split('-');
            const [left, right] = time.split('.');
            const [hour, minute, second] = left.split(':');
            const seconds =
                new Date(
                    Date.UTC(
                        Number.parseInt(year),
                        Number.parseInt(month) - 1,
                        Number.parseInt(day),
                        Number.parseInt(hour),
                        Number.parseInt(minute),
                        Number.parseInt(second)
                    )
                ).getTime() / 1000;
            const nanos = Number.parseInt(right.substring(0, 6)) * 1000;
            return new admin.firestore.Timestamp(seconds, nanos);
        } else {
            return admin.firestore.Timestamp.now();
        }
    }
}
