import * as fs from 'fs';
import {ExportService} from '@endran/firestore-export-import/dist/export.service';
import * as os from 'os';
import * as path from 'path';
import * as admin from 'firebase-admin';

export class FireAuditExporterService {
    static COUNTER = 0;

    constructor(
        private exportService: ExportService,
        private bucketName: string,
        private _fs = fs,
        private _path = path,
        private _os = os,
        private _admin = admin
    ) {
    }

    private getBucket() {
        return this._admin.storage().bucket(this.bucketName);
    }

    async export(firestorePath: string, storagePath: string, timestamp: string, transformer: (data: any, path: string) => any): Promise<any> {
        const dir = `${this._os.tmpdir()}/dir${FireAuditExporterService.COUNTER++}`;

        const filename = this.createFilename(firestorePath, timestamp);
        this._fs.mkdirSync(dir);
        const localPath = this._path.join(dir, filename);

        console.log(`Streaming data into ${localPath}`);
        this._fs.openSync(localPath, 'w');
        const writeStream = this._fs.createWriteStream(localPath);

        try {
            await this.exportService.export(firestorePath, writeStream, timestamp, transformer);
            console.log(`Streaming done`);
        } catch (e) {
            console.error('Could not export data', e);
            throw Error('Could not export data');
        }
        writeStream.close();

        try {
            console.log(`Start upload`);
            const cloudPath = `${storagePath}/${timestamp}/${filename}`;
            await this.uploadFile(localPath, cloudPath, {contentType: 'application/json'});
            console.log(`Upload done: ${cloudPath}`);
        } catch (e) {
            console.error('Could not upload data', e);
            throw Error('Could not upload data');
        }

        this._fs.unlinkSync(localPath);
    }

    private createFilename(firestorePath: string, timestamp: string) {
        let specification = 'root';
        if (firestorePath) {
            let val = firestorePath.startsWith('/') ? firestorePath.replace('/', '') : firestorePath;
            val = val.endsWith('/') ? val.substring(0, val.length - 1) : val;
            specification = val.replace(/([^a-z0-9-_]+)/gi, '-');
        }

        return `backup_${specification}_${timestamp}.json`;
    }

    private async uploadFile(localPath: string, cloudPath: string, metadata?: any): Promise<any> {
        const uploadResponse = await this.getBucket().upload(localPath, {
            destination: cloudPath,
            metadata: metadata,
            resumable: false
        });
        return uploadResponse[1];
    }
}
