import {FireAuditExporterService} from './fireaudit-exporter.service';
import {ExportService} from '@endran/firestore-export-import/dist/export.service';
import {JestMock, testResolve} from '../fireaudit.spec';
import {WriteStream} from "fs";

describe('FireAuditExporterService', () => {
    let service: FireAuditExporterService;

    let exportServiceMock: JestMock<ExportService>;
    let fsMock: JestMock<any>;
    let pathMock: JestMock<any>;
    let osMock: JestMock<any>;
    let adminMock: JestMock<any>;

    let storageMock: JestMock<any>;
    let bucketMock: JestMock<any>;
    let writeStreamMock: JestMock<WriteStream>;

    beforeEach(() => {
        exportServiceMock= new JestMock('export');

        writeStreamMock = new JestMock('close');
        fsMock= new JestMock('mkdirSync', 'openSync', 'createWriteStream', 'unlinkSync');
        fsMock.fn.createWriteStream.mockReturnValue(writeStreamMock.typed);

        pathMock= new JestMock('join');
        pathMock.fn.join.mockReturnValue('TEST_JOIN');

        osMock= new JestMock('tmpdir');
        osMock.fn.tmpdir.mockReturnValue('TEST_TMPDIR');

        bucketMock= new JestMock('upload');
        storageMock= new JestMock('bucket');
        adminMock= new JestMock('storage');

        bucketMock.fn.upload.mockReturnValue(testResolve(['ZERO','ONE','TWO']));
        storageMock.fn.bucket.mockReturnValue(bucketMock.typed);
        adminMock.fn.storage.mockReturnValue(storageMock.typed);

        FireAuditExporterService.COUNTER = 111;

        service = new FireAuditExporterService(
            exportServiceMock.typed,
            'TEST_BUCKET_NAME',
            fsMock.typed,
            pathMock.typed,
            osMock.typed,
            adminMock.typed
        );
    });

    describe('export', () => {
        test('has proper disk handling', async () => {
            await service.export(undefined, undefined, 'TEST_TIMESTAMP', undefined);

            expect(osMock.fn.tmpdir).toHaveBeenCalled();
            expect(fsMock.fn.mkdirSync).toHaveBeenCalledWith('TEST_TMPDIR/dir111');
            expect(pathMock.fn.join).toHaveBeenCalledWith('TEST_TMPDIR/dir111','backup_root_TEST_TIMESTAMP.json');
            expect(fsMock.fn.openSync).toHaveBeenCalledWith('TEST_JOIN', 'w');
            expect(fsMock.fn.createWriteStream).toHaveBeenCalledWith('TEST_JOIN');
            expect(writeStreamMock.fn.close).toHaveBeenCalled();
            expect(fsMock.fn.unlinkSync).toHaveBeenCalled();
        });

        test('creates filename named after firestorePath', async () => {
            await service.export('TEST/PATH', undefined, 'TEST_TIMESTAMP', undefined);

            expect(pathMock.fn.join).toHaveBeenCalledWith('TEST_TMPDIR/dir111','backup_TEST-PATH_TEST_TIMESTAMP.json');
        });

        test('stripts leading and trailing / from firestorePath', async () => {
            await service.export('/TEST/PATH/TEST/PATH/', undefined, 'TEST_TIMESTAMP', undefined);

            expect(pathMock.fn.join).toHaveBeenCalledWith('TEST_TMPDIR/dir111','backup_TEST-PATH-TEST-PATH_TEST_TIMESTAMP.json');
        });

        test('calls export service', async () => {
            const transformer = d => d;
            await service.export('TEST_PATH', undefined, 'TEST_TIMESTAMP', transformer);

            expect(exportServiceMock.fn.export).toHaveBeenCalledWith('TEST_PATH',writeStreamMock.typed, 'TEST_TIMESTAMP', transformer);
        });

        test('uploads result', async () => {
            await service.export( undefined, 'TEST_CLOUD_PATH', 'TEST_TIMESTAMP', undefined);

            expect(adminMock.fn.storage).toHaveBeenCalled();
            expect(storageMock.fn.bucket).toHaveBeenCalledWith('TEST_BUCKET_NAME');
            expect(bucketMock.fn.upload).toHaveBeenCalledWith('TEST_JOIN', {
                destination: 'TEST_CLOUD_PATH/TEST_TIMESTAMP/backup_root_TEST_TIMESTAMP.json',
                metadata: {contentType: 'application/json'},
                resumable: false
            });
        });
    });


});
