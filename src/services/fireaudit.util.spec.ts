import {FireAuditUtil} from './fireaudit.util';
import * as CryptoJS from 'crypto-js';

describe('FireAuditUtil', () => {

    function hash(data: string, salt: string): string {
        return CryptoJS.SHA512(data + salt).toString();
    }

    describe('hash', () => {
        test('hashes data with default salt when no salt is provided', async () => {
            const fireAuditUtil = new FireAuditUtil('TEST_DEFAULT_SALT', undefined, undefined);

            const actual = fireAuditUtil.hash('TEST_DATA');

            expect(actual).toEqual(hash('TEST_DATA', 'TEST_DEFAULT_SALT'));
        });

        test('hashes data with provided salt, even when there is no default', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, undefined, undefined);

            const actual = fireAuditUtil.hash('TEST_DATA', 'TEST_SALT');

            expect(actual).toEqual(hash('TEST_DATA', 'TEST_SALT'));
        });

        test('hashes data with provided salt, even though default is set', async () => {
            const fireAuditUtil = new FireAuditUtil('TEST_DEFAULT_SALT', undefined, undefined);

            const actual = fireAuditUtil.hash('TEST_DATA', 'TEST_SALT');

            expect(actual).toEqual(hash('TEST_DATA', 'TEST_SALT'));
        });

        test('throws error when hashing without default or provided data salt', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, undefined, undefined);

            await expect(() => fireAuditUtil.hash('TEST_DATA')).toThrowError();
        });
    });

    describe('encryption', () => {
        test('to throw when no password is provided during encryption', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, undefined, undefined);
            await expect(() => fireAuditUtil.encrypt('TEST_DATA')).toThrowError();
        });
        test('to throw when no password is provided during decryption', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, undefined, undefined);
            await expect(() => fireAuditUtil.decrypt('TEST_DATA')).toThrowError();
        });

        test('securely encrypt and decrypt using constructor provided encryptionPassword', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, 'TEST_PASSWORD', undefined);

            const expected = 'TEST_STRING';

            const cypher1 = fireAuditUtil.encrypt(expected);
            const cypher2 = fireAuditUtil.encrypt(expected);
            const actual1 = fireAuditUtil.decrypt(cypher1);
            const actual2 = fireAuditUtil.decrypt(cypher2);

            expect(actual1).toEqual(expected);
            expect(actual2).toEqual(expected);
            expect(actual1).not.toEqual(cypher1);
            expect(actual2).not.toEqual(cypher2);

            // Cyphers of the same message must differ to be secure
            expect(cypher1).not.toEqual(cypher2);
        });

        test('securely encrypt and decrypt using param provided passwords', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, undefined, undefined);

            const expected = 'TEST_STRING';
            const cypher = fireAuditUtil.encrypt(expected, 'PARAM_PASSWORD');
            const actual = fireAuditUtil.decrypt(cypher, ['PW1', 'PARAM_PASSWORD', 'PW3']);

            expect(actual).toEqual(expected);
            expect(actual).not.toEqual(cypher);
        });

        test('to throw when decryption password cannot be found', async () => {
            const fireAuditUtil = new FireAuditUtil(undefined, undefined, undefined);

            const expected = 'TEST_STRING';
            const cypher = fireAuditUtil.encrypt(expected, 'PARAM_PASSWORD');

            await expect(() => fireAuditUtil.decrypt(cypher, ['PW1', 'PW2', 'PW3'])).toThrowError();
        });
    });
});
