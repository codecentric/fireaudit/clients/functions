import * as CryptoJS from 'crypto-js';

export class FireAuditUtil {
    private encryptionPassword: string;
    private decryptionPasswords: string[];

    constructor(private sha512Salt: string, encryptionPassword: string, decryptionPasswords: string[] = []) {
        this.encryptionPassword = encryptionPassword;
        this.decryptionPasswords = !!encryptionPassword ? [encryptionPassword, ...decryptionPasswords] : undefined;
    }

    /**
     * Hash data, using sha512 and the provided salt.
     * @param data          The `string` data to be hashed.
     * @param sha512Salt    Hashing salt, defaults to `FireAuditOptions.hashing.sha512Salt`
     */
    hash(data: string, sha512Salt: string = this.sha512Salt): string {
        if (!sha512Salt) {
            throw Error('hash sha512Salt not set');
        }
        return CryptoJS.SHA512(data + sha512Salt).toString();
    }

    encrypt(clearText: string, encryptionPassword: string = this.encryptionPassword): string {
        if (!encryptionPassword) {
            throw Error('encrypt encryptionPassword not set');
        }

        return CryptoJS.AES.encrypt(`FIRE_AUDIT_${clearText}`, encryptionPassword).toString();
    }

    decrypt(cypherText: string, decryptionPasswords: string[] = this.decryptionPasswords): string {
        if (!decryptionPasswords) {
            throw Error('decrypt decryptionPasswords not set');
        }

        for (const decryptionPassword of decryptionPasswords) {
            try {
                const bytes = CryptoJS.AES.decrypt(cypherText, decryptionPassword);
                const originalText = bytes.toString(CryptoJS.enc.Utf8);
                if (originalText && originalText.startsWith('FIRE_AUDIT_')) {
                    return originalText.substring(11);
                }
            } catch (e) {
            }
        }

        throw Error('No valid decryptionPassword provided');
    }
}
